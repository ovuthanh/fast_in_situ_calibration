function [ T , RMSE , MERs ] = IN_Cal( X, X_theo, W, F_theo, Omega_G, Omega_F, Phi_G, Phi_F, G, F, config )

%% loading the config parameters 
Tmax = config.Tmax;
delta_measure = config.delta_measure;

%% 

num_sensor = config.numSensor;

W2 = W.^2;
Omega_Bar_G = ones(size(G))-Omega_G;
Omega_Bar_F = ones(size(F))-Omega_F;
Delta_G = G.*Omega_Bar_G;
Delta_F = F.*Omega_Bar_F;

iter_max = round(Tmax / delta_measure) ;
T = nan(1,iter_max);
RMSE = nan(1+config.numSubSensor,iter_max);
MERs = nan(1+config.numSubSensor,iter_max);

tic
i = 1;
T(i) = toc;
RMSE(:,i) = vecnorm(F(:,1:num_sensor)- F_theo(:,1:num_sensor),2,2)/sqrt(num_sensor);

while toc<Tmax
    
    % updating G
    Delta_G = Updt_Delta_G( W2 , X , Delta_G , Phi_G , F , Omega_Bar_G );
    G = Phi_G + Delta_G;
    
    % updating F
    Delta_F = Updt_Delta_F( W2 , X , G , Delta_F , Phi_F , Omega_Bar_F );
    F = Phi_F + Delta_F;
    
    if toc - i*delta_measure >= delta_measure
        i = i+1;
        if i > iter_max
            break
        end
        [MER,~]=bss_eval_mix(F_theo',F');
        MERs(:,i) = MER;
        T(i) = toc;
        RMSE(:,i) = vecnorm(F(:,1:num_sensor)- F_theo(:,1:num_sensor),2,2)/sqrt(num_sensor);
    end
    
end

end



function [ Delta_F ] = Updt_Delta_F( W , X , G , Delta_F , Phi_F , Omega_B_F )
%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update rule for Delta_F %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
Delta_F = Delta_F.*(Omega_B_F).*((G'*(W.*secu_plus(X-G*Phi_F)))./(G'*(W.*(G*Delta_F))));
Delta_F(isnan(Delta_F))=0;
end



function [ Delta_G ] = Updt_Delta_G( W , X , Delta_G , Phi_G , F , Omega_B_G )
%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update rule for Delta_G %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
Delta_G = Delta_G.*(Omega_B_G).*((W.*secu_plus(X-Phi_G*F))*F')./((W.*(Delta_G*F)*F')); % mise à jour
Delta_G(isnan(Delta_G))=0;
end



function [toto] = secu_plus(tutu,s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Goal: Security in the NMF procedure which project the negative data to
% epsilon (small user-defined threshold
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(nargin<2)
    s=1.e-12;
end
toto=max(tutu,s);
toto(isnan(tutu)) = 0;
end