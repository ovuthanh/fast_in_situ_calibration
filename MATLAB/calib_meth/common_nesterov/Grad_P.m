function GradP = Grad_P( Grad , X )

GradP=norm(Grad(Grad<0|X>0));