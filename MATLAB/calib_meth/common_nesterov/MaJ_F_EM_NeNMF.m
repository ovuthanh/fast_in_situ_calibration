function [ F1 , i ] = MaJ_F_EM_NeNMF( GG , GX , F1 , InnerMinIter , InnerMaxIter , StoppingCritF , nOmega_F)

Y = F1;

alpha=zeros(2,1);
alpha(1) = 1;

Grad_y = GG*Y-GX;

L = norm(GG);

for i = 1 : InnerMaxIter
    
    F2 = max(Y-(1/L)*Grad_y,0).*nOmega_F;
%     F2 = F2.*nOmega_F; % Projection on not(Omega_F)
    alpha(2) = (1+sqrt(4*alpha(1)^2+1))/2;
    
    Y = F2 + ((alpha(1)-1)/alpha(2))*(F2-F1);
    Grad_y = GG*Y-GX;
    
    F1 = F2;
    alpha(1) = alpha(2);
    
%     if mod(i , InnerMinIter) == 0
%         if(Grad_P(Grad_y , Y)<=StoppingCritF)
%             break
%         end
%     end
    
end
end