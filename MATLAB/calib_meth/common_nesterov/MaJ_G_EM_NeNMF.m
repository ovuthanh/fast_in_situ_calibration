function [ G1 , i ] = MaJ_G_EM_NeNMF( FF , XF , G1 , InnerMinIter , InnerMaxIter , StoppingCritG , nOmega_G)

Y = G1;

alpha = zeros(2,1);
alpha(1) = 1;

L = norm(FF);

Grad_y = Y*FF-XF;

for i = 1 : InnerMaxIter
    
    G2 = max(Y-(1/L)*Grad_y,0).*nOmega_G;
%     G2 = G2.*nOmega_G; % Projection on not(Omega_G)
    alpha(2) = (1+sqrt(4*alpha(1)^2+1))/2;
    
    Y = G2 + ((alpha(1)-1)/alpha(2))*(G2-G1);
    Grad_y = Y*FF-XF;
    
    G1 = G2;
    alpha(1) = alpha(2);
    
%     if mod(i , InnerMinIter) == 0
%         if(Grad_P(Grad_y , Y)<=StoppingCritG)
%             break
%         end
%     end
    
end
end

