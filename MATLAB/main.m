clear all

%% Adding every files in the path

addpath(genpath(pwd))

%% run config with maag et al values of the phenomenon

% run_config("config3.json")
% res = run_config("config.json");
% run_config("config4.json") % Not working for IN_CAL ???
% run_config("config2.json")
% run_config_forget_other_sen("config3.json")

% run_config("SNR_100dB.json")
% run_config("SNR_infdB.json")

% res = run_config("config10x10.json");
res = run_config("test_rapport.json");
% res = run_config("30x30farincal.json");