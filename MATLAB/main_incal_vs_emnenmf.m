clear
close all
clc

%% Adding every files in the path

addpath(genpath(pwd))

%% Simulation parameters

s_width = 50;  % Scene width
s_length = 50; % Scene length
N_Ref = 4; % Nb. of reference measurements
N_Cpt = 100; % Nb. of mobile sensors
Mu_beta = .9; % Mean sensors gain
Mu_alpha = 5; % Mean sensors offset
Bound_beta = [.01;1.5]; % Gain boundaries
Bound_alpha = [3.5;6.5]; % Offset boundaries

MV = .5; % Missing Value prop.
RV = 0.3; % RendezVous prop.
var_n = 0; % Noise variance
M_loop = 50; % Number of M loop per E step
runs = 1; % Total number of runs

%% Nesterov parameters
InnerMinIter = 5;
InnerMaxIter = 50;
Tmax = 30;

%% 
delta_measure = 1;
iter_max = round(Tmax / delta_measure);


%% Allocation for the RMSE values
% emnenmf
RMSE_offset_emnenmf = nan(runs, iter_max);
RMSE_gain_emnenmf = nan(runs, iter_max);
% Incal
RMSE_offset_incal = nan(runs, iter_max);
RMSE_gain_incal = nan(runs, iter_max);

for run = 1:runs
    % data generation
    [X, X_theo, W, F_theo, Omega_G, Omega_F, Phi_G, Phi_F, Ginit, Finit] = data_gen(s_width, s_length, run, N_Ref, N_Cpt, Mu_beta, Mu_alpha, Bound_beta, Bound_alpha, MV, RV, var_n);
    % emnenmf
    [T_emnenmf, RMSE] = emnenmf( W , X , Ginit , Finit, Omega_G, Omega_F, Phi_G, Phi_F , InnerMinIter , InnerMaxIter , Tmax , M_loop, F_theo, delta_measure);
    RMSE_offset_emnenmf(run,:) = RMSE(1,:);
    RMSE_gain_emnenmf(run,:) = RMSE(2,:);
    % incal
    [T_incal, RMSE] = IN_Cal( W , X , Ginit , Finit , Omega_G , Omega_F , Phi_G , Phi_F , F_theo , Tmax, delta_measure );
    RMSE_offset_incal(run,:) = RMSE(1,:);
    RMSE_gain_incal(run,:) = RMSE(2,:);    
    
end

% emnenmf
min_offset_emnenmf = min(RMSE_offset_emnenmf,[],1,'omitnan');
med_offset_emnenmf = median(RMSE_offset_emnenmf,1,'omitnan');
max_offset_emnenmf = max(RMSE_offset_emnenmf,[],1,'omitnan');

min_gain_emnenmf = min(RMSE_gain_emnenmf,[],1,'omitnan');
med_gain_emnenmf = median(RMSE_gain_emnenmf,1,'omitnan');
max_gain_emnenmf = max(RMSE_gain_emnenmf,[],1,'omitnan');

subplot(121)
semilogy(T_emnenmf,min_offset_emnenmf,'b')
hold on
semilogy(T_emnenmf,med_offset_emnenmf,'b')
o_e = semilogy(T_emnenmf,max_offset_emnenmf,'b');
hold off

subplot(122)
semilogy(T_emnenmf,min_gain_emnenmf,'b')
hold on
semilogy(T_emnenmf,med_gain_emnenmf,'b')
g_e = semilogy(T_emnenmf,max_gain_emnenmf,'b');
hold off

% incal
min_offset_incal = min(RMSE_offset_incal,[],1,'omitnan');
med_offset_incal = median(RMSE_offset_incal,1,'omitnan');
max_offset_incal = max(RMSE_offset_incal,[],1,'omitnan');

min_gain_incal = min(RMSE_gain_incal,[],1,'omitnan');
med_gain_incal = median(RMSE_gain_incal,1,'omitnan');
max_gain_incal = max(RMSE_gain_incal,[],1,'omitnan');

subplot(121)
hold on
semilogy(T_incal,min_offset_incal,'r')
semilogy(T_incal,med_offset_incal,'r')
o_i = semilogy(T_incal,max_offset_incal,'r');
hold off

subplot(122)
hold on
semilogy(T_incal,min_gain_incal,'r')
semilogy(T_incal,med_gain_incal,'r')
g_i = semilogy(T_incal,max_gain_incal,'r');
hold off

% adding title and labels
subplot(121)
title('RMSE offset')
legend([o_e o_i],'EMNeNMF','IN\_Cal')

subplot(122)
title('RMSE gain')
legend([g_e g_i],'EMNeNMF','IN\_Cal')

med_offset_incal(end)
med_offset_emnenmf(end)