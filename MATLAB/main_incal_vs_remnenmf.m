clear
close all
clc

%% Adding every files in the path

addpath(genpath(pwd))

%% Simulation parameters

s_width = 50;  % Scene width
s_length = 50; % Scene length
N_Ref = 4; % Nb. of reference measurements
N_Cpt = 100; % Nb. of mobile sensors
Mu_beta = .9; % Mean sensors gain
Mu_alpha = 5; % Mean sensors offset
Bound_beta = [.01;1.5]; % Gain boundaries
Bound_alpha = [3.5;6.5]; % Offset boundaries

MV = .9; % Missing Value prop.
RV = 0.3; % RendezVous prop.
var_n = 0; % Noise variance
M_loop = 50; % Number of M loop per E step
runs = 1; % Total number of runs

%% Nesterov parameters
InnerMinIter = 5;
InnerMaxIter = 20;
Tmax = 300;

%% Compression parameters
nu = 10;

%% 
delta_measure = 1;
iter_max = round(Tmax / delta_measure);


%% Allocation for the RMSE values
% remnenmf
RMSE_offset_remnenmf = nan(runs, iter_max);
RMSE_gain_remnenmf = nan(runs, iter_max);
% Incal
RMSE_offset_incal = nan(runs, iter_max);
RMSE_gain_incal = nan(runs, iter_max);

for run = 1:runs
    % data generation
    [X, X_theo, W, F_theo, Omega_G, Omega_F, Phi_G, Phi_F, Ginit, Finit] = data_gen(s_width, s_length, run, N_Ref, N_Cpt, Mu_beta, Mu_alpha, Bound_beta, Bound_alpha, MV, RV, var_n);
    seed = floor(rand*10000);
    % remnenmf
    rng(seed)
    [T_remnenmf, RMSE] = remnenmf_not_full( W , X , Ginit , Finit, Omega_G, Omega_F, Phi_G, Phi_F , InnerMinIter , InnerMaxIter , Tmax , M_loop, F_theo, delta_measure, nu);
    RMSE_offset_remnenmf(run,:) = RMSE(1,:);
    RMSE_gain_remnenmf(run,:) = RMSE(2,:);
    % incal
    rng(seed)
    [T_incal, RMSE] = IN_Cal( W , X , Ginit , Finit , Omega_G , Omega_F , Phi_G , Phi_F , F_theo , Tmax, delta_measure );
    RMSE_offset_incal(run,:) = RMSE(1,:);
    RMSE_gain_incal(run,:) = RMSE(2,:);    
    
end

% remnenmf
min_offset_remnenmf = min(RMSE_offset_remnenmf,[],1,'omitnan');
med_offset_remnenmf = median(RMSE_offset_remnenmf,1,'omitnan');
max_offset_remnenmf = max(RMSE_offset_remnenmf,[],1,'omitnan');

min_gain_remnenmf = min(RMSE_gain_remnenmf,[],1,'omitnan');
med_gain_remnenmf = median(RMSE_gain_remnenmf,1,'omitnan');
max_gain_remnenmf = max(RMSE_gain_remnenmf,[],1,'omitnan');

subplot(121)
semilogy(T_remnenmf,min_offset_remnenmf,'b')
hold on
semilogy(T_remnenmf,med_offset_remnenmf,'b')
o_e = semilogy(T_remnenmf,max_offset_remnenmf,'b');
hold off

subplot(122)
semilogy(T_remnenmf,min_gain_remnenmf,'b')
hold on
semilogy(T_remnenmf,med_gain_remnenmf,'b')
g_e = semilogy(T_remnenmf,max_gain_remnenmf,'b');
hold off

% incal
min_offset_incal = min(RMSE_offset_incal,[],1,'omitnan');
med_offset_incal = median(RMSE_offset_incal,1,'omitnan');
max_offset_incal = max(RMSE_offset_incal,[],1,'omitnan');

min_gain_incal = min(RMSE_gain_incal,[],1,'omitnan');
med_gain_incal = median(RMSE_gain_incal,1,'omitnan');
max_gain_incal = max(RMSE_gain_incal,[],1,'omitnan');

subplot(121)
hold on
semilogy(T_incal,min_offset_incal,'r')
semilogy(T_incal,med_offset_incal,'r')
o_i = semilogy(T_incal,max_offset_incal,'r');
hold off

subplot(122)
hold on
semilogy(T_incal,min_gain_incal,'r')
semilogy(T_incal,med_gain_incal,'r')
g_i = semilogy(T_incal,max_gain_incal,'r');
hold off

% adding title and labels
subplot(121)
title('RMSE offset')
legend([o_e o_i],'REMNeNMF','IN\_Cal')

subplot(122)
title('RMSE gain')
legend([g_e g_i],'REMNeNMF','IN\_Cal')

