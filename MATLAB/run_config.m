function [RMSEs] = run_config(varargin)
    if nargin == 0
        file_name = "default_config.json";
    elseif nargin == 1
        file_name = varargin{1};
    else
        file_name = varargin{1};
        warning("run_config only accepts one argument. Every other argument is ignored")
    end
    config = jsondecode(fileread(file_name));
    methods = config.calibrationMethods;
    iter_max = round(config.Tmax / config.delta_measure);
    RMSEs = cell(length(methods),1+config.numSubSensor);
    times = cell(length(methods),1);
    
    for run = 1:config.numRuns
        % data generation
        [X, X_theo, W, F_theo, Omega_G, Omega_F, Phi_G, Phi_F, Ginit, Finit] = data_gen(config, run);
        seed = floor(rand*10000);
        
        % We want to only calibrate the sensor(s) of interest according to config.CalibratedSensor
        idx_to_calibrate = kron(config.CalibratedSensor'-1,(config.numSensor+1)*ones(1,config.numSensor+1))+kron(ones(1,numel(config.CalibratedSensor)),1:config.numSensor+1);
        X = X(:,idx_to_calibrate);
        X_theo = X_theo(:,idx_to_calibrate);
        W = W(:,idx_to_calibrate);
        F_theo = F_theo(:,idx_to_calibrate);
        Omega_F = Omega_F(:,idx_to_calibrate);
        Phi_F = Phi_F(:,idx_to_calibrate);
        Finit = Finit(:,idx_to_calibrate);

%         X = X(:,1:config.numSensor+1);
%         X_theo = X_theo(:,1:config.numSensor+1);
%         W = W(:,1:config.numSensor+1);
%         F_theo = F_theo(1:2,1:config.numSensor+1);
%         Omega_F = Omega_F(1:2,1:config.numSensor+1);
%         Phi_F = Phi_F(1:2,1:config.numSensor+1);
%         Finit = Finit(1:2,1:config.numSensor+1);
%         Omega_G = Omega_G(:,1:2);
%         Phi_G = Phi_G(:,1:2);
%         Ginit = Ginit(:,1:2);
%         config.numSubSensor = 1;
        
        % Testing each method for the same data
        for it_meth = 1:length(methods)
            rng(seed) % resetting the seed before starting the method
            fct = str2func(methods{it_meth});
            [T, RMSE , MERs] = fct(X, X_theo, W, F_theo, Omega_G, Omega_F, Phi_G, Phi_F, Ginit, Finit, config);
            
            times{it_meth} = T; % overwritting previous record times for simplicity's sake
%             figure
            for measure = 1:(1+config.numSubSensor)
                RMSEs{it_meth,measure} = cat(1, RMSEs{it_meth,measure}, RMSE(measure,:)); % saving the RMSE 
%                 % RMSEs{it_meth,1} is for the offsets, RMSEs{it_meth,>1} is
%                 % for the gains
%                 plot(MERs(measure,:))
%                 hold on
            end
        end
    end
    if config.display
        figure
        colors = ['b','r','g','k','y','c','m'];
        for it_meth = 1:length(methods)
            for measure = 2 % Only the RMSE for the gain
%                 subplot(1,config.numSubSensor+1,measure) % Only the RMSE for the gain
                enveloppe_min = min(RMSEs{it_meth,measure},[],1);
                enveloppe_max = max(RMSEs{it_meth,measure},[],1);
                semilogy(enveloppe_max,colors(it_meth),'HandleVisibility','off');
                hold on
                semilogy(enveloppe_min,colors(it_meth),'DisplayName',"Enveloppe "+strrep(methods{it_meth},'_','\_'));
                legend show
            end
        end
        for it_meth = 1:length(methods)
            for measure = 2 % Only the RMSE for the gain
                med = median(RMSEs{it_meth,measure});
                semilogy(med,"--"+colors(it_meth),'LineWidth',3,'DisplayName',"M�diane "+strrep(methods{it_meth},'_','\_'));
                hold on
                legend show
            end
        end 
        xlabel("Time (s)")
        ylabel("RMSE")
        grid on
        set(gca,'FontUnits','points','FontSize',18,'FontName','Times')
        fig = gcf;
        legend('AutoUpdate','off')
        Lines = findall(gcf,'Type','line');
        nenv = numel(Lines)/3;
        for k = 1:nenv
            line1 = Lines(3*nenv-(k-1)*2);
            line2 = Lines(3*nenv-(k-1)*2-1);
            fill([line1.XData flip(line2.XData)],[line1.YData flip(line2.YData)],colors(k),'LineStyle','none')
        end

        alpha(0.25)
%         for it_meth = 1:length(methods)
%             for measure = 2 % Only the RMSE for the gain
% %                 subplot(1,config.numSubSensor+1,measure) % Only the RMSE for the gain
%                 for k = 1:size(RMSEs{it_meth,measure},1)
%                     if k == size(RMSEs{it_meth,measure},1)
%                         semilogy(RMSEs{it_meth,measure}(k,:),colors(it_meth),'DisplayName',"Enveloppe "+strrep(methods{it_meth},'_','\_'));
%                     else
%                         semilogy(RMSEs{it_meth,measure}(k,:),colors(it_meth),'HandleVisibility','off');
%                     end
%                     hold on
%                 end
%                 legend show
%                 set(gca,'FontUnits','points','FontSize',12,'FontName','Times')
%             end
%         end
    end

%     if config.display
%         figure
%         for it_meth = 1:length(methods)
%             for measure = 1:(config.numSubSensor+1)
%                 subplot(length(methods),config.numSubSensor+1,(it_meth-1)*(config.numSubSensor+1)+measure)
%                 for k = 1:size(RMSEs{it_meth,measure},1)
%                     semilogy(RMSEs{it_meth,measure}(k,:))
%                     hold on
%                 end
%                 hold off
%             end
%         end
%     end
end

% set(gcf,'OuterPosition',[200 200 400 440]);
% axis square
% pos = get(gcf,'paperposition');
% set(gcf,'paperposition',[pos(1),pos(2), 4, 4]);

% print -depsc epsFig

