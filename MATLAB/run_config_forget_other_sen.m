function [res] = run_config_forget_other_sen(varargin)
    if nargin == 0
        file_name = "default_config.json";
    elseif nargin == 1
        file_name = varargin{1};
    else
        file_name = varargin{1};
        warning("run_config only accepts one argument. Every other argument is ignored")
    end
    config = jsondecode(fileread(file_name));
    methods = config.calibrationMethods;
    iter_max = round(config.Tmax / config.delta_measure);
    RMSEs = cell(length(methods),1+config.numSubSensor);
    times = cell(length(methods),1);
    
    for run = 1:config.numRuns
        % data generation
        [X, X_theo, W, F_theo, Omega_G, Omega_F, Phi_G, Phi_F, Ginit, Finit] = data_gen(config, run);
        seed = floor(rand*10000);
        
        % We want to only calibrate the sensor of interest
%         X = X(:,1:config.numSensor+1);
%         X_theo = X_theo(:,1:config.numSensor+1);
%         W = W(:,1:config.numSensor+1);
%         F_theo = F_theo(:,1:config.numSensor+1);
%         Omega_F = Omega_F(:,1:config.numSensor+1);
%         Phi_F = Phi_F(:,1:config.numSensor+1);
%         Finit = Finit(:,1:config.numSensor+1);

        X = X(:,1:config.numSensor+1);
        X_theo = X_theo(:,1:config.numSensor+1);
        W = W(:,1:config.numSensor+1);
        F_theo = F_theo(1:2,1:config.numSensor+1);
        Omega_F = Omega_F(1:2,1:config.numSensor+1);
        Phi_F = Phi_F(1:2,1:config.numSensor+1);
        Finit = Finit(1:2,1:config.numSensor+1);
        Omega_G = Omega_G(:,1:2);
        Phi_G = Phi_G(:,1:2);
        Ginit = Ginit(:,1:2);
        config.numSubSensor = 1;
        
        % Testing each method for the same data
        for it_meth = 1:length(methods)
            rng(seed) % resetting the seed before starting the method
            fct = str2func(methods{it_meth});
            [T, RMSE , MERs] = fct(X, X_theo, W, F_theo, Omega_G, Omega_F, Phi_G, Phi_F, Ginit, Finit, config);
            
            times{it_meth} = T; % overwritting previous record times for simplicity's sake
%             figure
            for measure = 1:(1+config.numSubSensor)
                RMSEs{it_meth,measure} = cat(1, RMSEs{it_meth,measure}, RMSE(measure,:)); % saving the RMSE 
%                 % RMSEs{it_meth,1} is for the offsets, RMSEs{it_meth,>1} is
%                 % for the gains
%                 plot(MERs(measure,:))
%                 hold on
            end
        end
    end
    if config.display
        figure
        for it_meth = 1:length(methods)
            for measure = 1:(config.numSubSensor+1)
                subplot(length(methods),config.numSubSensor+1,(it_meth-1)*(config.numSubSensor+1)+measure)
                for k = 1:size(RMSEs{it_meth,measure},1)
                    semilogy(RMSEs{it_meth,measure}(k,:))
                    hold on
                end
                hold off
            end
        end
    end
end

