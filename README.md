Une version Python et une version MATLAB sont disponibles. La version MATLAB est la plus à jour. La version Python a été mise de côté compte tenu de la lenteur non expliquée de la méthode EM par rapport à la méthode MU.

###### Version MATLAB ######

Le code fonctionne sur la version R2018a.

L'organisation est la suivante :

MATLAB
	├─ calib_meth				# Contient les différentes méthodes d'étalonnage
	│	├─ common_nesterov		# Contient des fonctions communes à EMNeNMF et REMNeNMF
	│	├─ EMNeNMF				# Contient la méthode EMNeNMF
	│	├─ INCAL				# Contient la méthode INCAL
	│	└─ REMNeNMF				# Contient la méthode REMNeNMF
	│
	├─ common
	│	└─ data_gen.m			# Fonction permettant de générer les données
	│
	├─ config.json				# Fichier permettant de configurer les données à générer et les méthodes
	└─ main.m					# Fichier exécutant le test en fonction de config.json
	
Pour configurer un test, les variables à paramétrer sont les suivantes : 